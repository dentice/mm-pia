import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../models/User';
import { UserInfo } from '../models/UserInfo';
const headerDict = {
    'accept': 'application/json',
}
const requestOptions = {                                                                                                                                                                                 
    headers: new HttpHeaders(headerDict)
};

@Injectable({
    providedIn: 'root'
}) 
export class ApiService {

    constructor(private svc: HttpClient){ }

    getUserInfo(url : string, token: string): Observable<UserInfo>{
        let headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        });
        return this.svc.get<UserInfo>(`${url}/api/auth/userInfo`, {headers:headers});
    }
    /*getUser(username: string,token: string): Observable<User[]>{
        let headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        });
        let search = {"username": username}
        return this.svc.get<User[]>(`${environment.DEFEND}/api/v1/user?s?s=${search}`, {headers:headers})
    }*/

}
