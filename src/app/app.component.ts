import { Component, OnInit, Renderer2 } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { KnowledgeBaseService } from './entry/knowledge-base/knowledge-base.service';
import { LanguagesService } from './services/languages.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  URL_KEY : string = "server_url";

  constructor(private _renderer: Renderer2,
              private _http: HttpClient,
              private _knowledgeBaseService: KnowledgeBaseService,
              private _languagesService: LanguagesService) {
    this._knowledgeBaseService.loadData(this._http);
    const increaseContrast = localStorage.getItem('increaseContrast');
    if (increaseContrast === 'true') {
      this._renderer.addClass(document.body, 'pia-contrast');
    } else {
      this._renderer.removeClass(document.body, 'pia-contrast');
    }

    // Languages initialization
    this._languagesService.initLanguages();
    this._languagesService.getOrSetCurrentLanguage();

  }
  ngOnInit(): void {
    var backendUrl = localStorage.getItem(this.URL_KEY);
    if(!backendUrl){
        var url = window.location.href;
        if(url.includes("defend-mm-pia-front"))
        {
            localStorage.setItem(this.URL_KEY, environment.PIA_PREPROD);
        }
        else if(url.includes("defend-prod-mm-pia-front"))
        {
            localStorage.setItem(this.URL_KEY, environment.PIA_PROD);
        }
        else
        {
          localStorage.setItem(this.URL_KEY, environment.PIA_TEST);
        }
    }
  }
}
