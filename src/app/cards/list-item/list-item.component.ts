import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { PARAMETERS } from '@angular/core/src/util/decorators';
import { Router, ActivatedRoute } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Attachment } from 'src/app/entry/attachments/attachment.model';
import { Pia } from 'src/app/entry/pia.model';

import { ModalsService } from 'src/app/modals/modals.service';
import { PiaService } from 'src/app/services/pia.service';
import { stubString } from 'cypress/types/lodash';
import { Structure } from 'src/app/structures/structure.model';
import { ApiService } from 'src/app/services/api.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: `.app-list-item`,
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit, AfterViewInit {
  @Input() pia: any;
  attachments: any;
  uriStart: String;
  paramQuery: string;
  boolAllRow: Boolean;
  jwtTokenRef: string;
  
   

  constructor(private router: Router,
              private route: ActivatedRoute,
              public _piaService: PiaService,
              private _modalsService: ModalsService,
              private apisvc: ApiService,
              private _http: HttpClient) {
              
               }
               ngAfterViewInit(){ }
               
                 
               ngOnInit()
{
    this.boolAllRow = true;
    const attachmentModel = new Attachment();
    attachmentModel.pia_id = this.pia.id;
    attachmentModel.findAll().then((entries: any) => {
          this.attachments = entries;
    });
}

  /**
   * Focuses out field and update PIA.
   * @param {string} attribute - Attribute of the PIA.
   * @param {*} event - Any Event.
   */
  onFocusOut(attribute: string, event: any) {
    const text = event.target.innerText;
    this.pia[attribute] = text;
    this.pia.update();
  }

  /**
   * Opens the modal to confirm deletion of a PIA
   * @param {string} id - The PIA id.
   */
  removePia(id: string) {
    localStorage.setItem('pia-id', id);
    this._modalsService.openModal('modal-remove-pia');
  }

  /**
   * Export the PIA
   * @param {number} id - The PIA id.
   */
  export(id: number) {
    this._piaService.export(id);
  }
}
