export interface UserInfo {
    iss:                string;
    sub:                string;
    name:               string;
    given_name:         string;
    family_name:        string;
    email:              string;
    preferred_username: string;
    groups:             string[];
}
