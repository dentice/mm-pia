export interface User {
    createDate:     Date;
    updateDate:     Date;
    id_user:        string;
    username:       string;
    fullname:       string;
    id_department:  null;
    id_company:     string;
    department:     null;
    company:        Company;
    actions:        any[];
    author_pias:    any[];
    assessor_pias:  any[];
    validator_pias: any[];
}

export interface Company {
    createDate:           Date;
    updateDate:           Date;
    id_company:           string;
    name:                 string;
    head_office_location: string;
    id_country:           string;
}
