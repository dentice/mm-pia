// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  name: 'development',
  production: false,
  version: 'de DEV',
  PIA_PREPROD: 'https://defend-mm-pia-back.preprod.pdmfc.com',
  PIA_PROD: 'https://defend-prod-mm-pia-back.preprod.pdmfc.com',
  PIA_TEST: 'https://defend-test-mm-pia-back.preprod.pdmfc.com',
  DEFEND_PREPROD: 'https://defend.preprod.pdmfc.com',
  DEFEND_PROD: 'https://defend-prod.preprod.pdmfc.com',
  DEFEND_TEST: 'https://defend-test.preprod.pdmfc.com',
  API: 'api/v1'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
